export const consts = {
	dbDir: './.database',
	tmpDir: './.temp',
	cfgPath: './config.yml',
	defaultPrefix: 'vega',
	defaultGuildSetting: {
		guildId: '0',
		lang: 0,
		triggers: []
	}
}