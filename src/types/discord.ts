export type Emote = {
	animated: boolean
	id: string
	name: string
	filename: string
	url: string
}